Meteor.methods({
  addUser: function(useratts) {
    console.log(useratts);
    var userId = Accounts.createUser({
      email: useratts.emails[0].address,
      password: useratts.profile.password,
      profile: {
        firstName: useratts.profile.firstName,
        lastName: useratts.profile.lastName,
        password: useratts.profile.password,
        roles: useratts.profile.roles,
        depositCard: useratts.profile.depositCard,
        city: useratts.profile.city
      }
    });
    Roles.addUserToRoles(userId, useratts.profile.roles);
    console.log(userId);
  },
  updateUser: function(useratts, documentId) {
    console.log(useratts);
    console.log(documentId);
    Users.update({_id: documentId}, useratts);
    var userpass = Users.findOne({_id: documentId}).profile.password;
    console.log(userpass);
    Accounts.setPassword(documentId, userpass);
  },
  impersonate: function(userId) {
    check(userId, String);
    this.setUserId(userId);
  }
});

