Meteor.startup(function () {
  if (Meteor.users.findOne( { emails: { $elemMatch: { address: 'stlouisweb@gmail.com' } } } ) == undefined) {
    $user = Accounts.createUser({
      username: 'jplack',
      password: 'password',
      email: 'stlouisweb@gmail.com',
      profile: {
        firstName: 'Jeremy',
        lastName: 'Plack',
        password: 'password',
        roles: 'admin'
      }
    });
    Roles.addUserToRoles($user, 'admin');
  };
  Meteor.publish("allUsers", function(){
    return Meteor.users.find();
  });
});



