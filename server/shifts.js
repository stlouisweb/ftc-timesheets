Meteor.methods({
  addDay: function(day) {

  },
  addLastWeek: function(user) {
    today = moment();
    lastWeek = [];
    lastWeek.push(today);
    for(i = 1; i < 8; i++){
      lastWeek.push(moment().subtract(i, 'd'));
    }
    _.each(lastWeek, function(value){
      Morning = {
        Day: value._d,
        TimePeriod: 'Morning',
        Period: 1,
        User: user,
        WeekOf: moment(value).format("w-YY")
      }
      Shifts.insert(Morning);
      Afternoon = {
        Day: value._d,
        TimePeriod: 'Afternoon',
        Period: 2,
        User: user,
        WeekOf: moment(value).format("w-YY")
      }
      Shifts.insert(Afternoon);
      Evening = {
        Day: value._d,
        TimePeriod: 'Evening',
        Period: 3,
        User: user,
        WeekOf: moment(value).format("w-YY")
      }
      Shifts.insert(Evening);
      Other = {
        Day: value._d,
        TimePeriod: 'Other',
        Period: 4,
        User: user,
        WeekOf: moment(value).format("w-YY")
      }
      Shifts.insert(Other);
    });
  },
  addTodaysShifts: function(user, today){
    var yesterday = moment(today).subtract(1, 'days');
    var tomorrow = moment(today).add(1, 'days');
    user = user.trim();
    var todaysShifts = Shifts.find({User: user, Day: {$gt: new Date(today), $lt: new Date(tomorrow)}}).fetch();
    if(user && todaysShifts.length === 0){
      today = moment(today).add(1, 'hours');;
      Morning = {
        Day: today._d,
        TimePeriod: 'Morning',
        Period: 1,
        User: user,
        WeekOf: moment(today).format("w-YY")
      }
      Shifts.insert(Morning);
      Afternoon = {
        Day: today._d,
        TimePeriod: 'Afternoon',
        Period: 2,
        User: user,
        WeekOf: moment(today).format("w-YY")
      }
      Shifts.insert(Afternoon);
      Evening = {
        Day: today._d,
        TimePeriod: 'Evening',
        Period: 3,
        User: user,
        WeekOf: moment(today).format("w-YY")
      }
      Shifts.insert(Evening);
      Other = {
        Day: today._d,
        TimePeriod: 'Other',
        Period: 4,
        User: user,
        WeekOf: moment(today).format("w-YY")
      }
      Shifts.insert(Other);
    }else{
      console.log('shifts exists');
    }

  }
});
//Accounts.onCreateUser(function(options, user) {
//  // add timesheet to user account
//  if (options.profile.roles == 'user'){
//    userId = user._id;
//    Meteor.call('addLastWeek', userId);
//    return user;
//  }
//});
//Accounts.onLogin(function(){
//  console.log("login ");
//})