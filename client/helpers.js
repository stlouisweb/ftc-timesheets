UI.registerHelper("getCurrentUserDisplayName", function(){
  return Meteor.user().profile.firstName + " " + Meteor.user().profile.lastName;
});
UI.registerHelper("fullDate", function(day){
  return moment(day).format('MM/DD/YYYY');
});
UI.registerHelper("day", function(day){
  return moment(day).format('dddd');
});
UI.registerHelper("Week", function(){
  var yesterday = moment().subtract(1, 'days').format('MM/DD/YYYY');
  var endOfTheWeek = moment().add(7, 'days').format('MM/DD/YYYY');
  return yesterday + ' - ' + endOfTheWeek;
});
UI.registerHelper("formatWeek", function(week){
  var weekarray = week.split('-');
  var week = weekarray[0];
  return moment().day("Sunday").week(week);
  var startOfWeek = moment(week);
});
UI.registerHelper("moment", function(){
  return moment().subtract(1, 'days').format('MM/DD/YYYY');
;});
UI.registerHelper("today", function(){
  return moment().format('MM/DD/YYYY');
});
UI.registerHelper("isAdmin", function(){
  user = Meteor.userId();
  return Roles.userHasRole(user, 'admin');
});
UI.registerHelper("test", function(){
  return Meteor.userId();
});
