Template.header.events({
  'click #signOut': function(event, template){
    Meteor.logout();
    Router.go('/');
  },
  'click .toggle-nav': function() {
    $('#mainNav').toggleClass('open');
    $('.page-wrapper').toggleClass('open');
  }
});