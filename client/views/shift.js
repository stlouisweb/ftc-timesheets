Template.shift.helpers({
  shiftFormId: function() {
    return "shift-form-" + this._id;
  },
  shiftArriveTime: function() {
    return "arrive-time-" + this._id;
  },
  shiftDepartTime: function() {
    return "depart-time-" + this._id;
  },
  shiftRate: function() {
    return "hourly-rate-" + this._id;
  },
  totalTime: function() {
    if(! Session.get('arrive-time-' + this._id)) {
      var arrive = moment($('#this-day').text() + " " + $('#arrive-time-' + this._id).val());
    }else{
      var arrive = moment(Session.get('arrive-time-' + this._id));
    }
    if(! Session.get('depart-time-' + this._id)){
      var depart = moment($('#this-day').text() + " " + $('#depart-time-' + this._id).val()).add(1, 'seconds');
    }else{
      var depart = moment(Session.get('depart-time-' + this._id));
    }
    totalTime = depart.diff(arrive);
    formatTime = moment.duration(totalTime).asHours();
    return formatTime;
  },
  amountDue: function() {
    if(! Session.get('hourly-rate-' + this._id)) {
      $( '#hourly-rate-' + this._id ).change(function() {
        var hourlyRate = $('#hourly-rate-' + this._id + ' option:selected').val();
        console.log(hourlyRate);
        return(hourlyRate);
      })
    }else{
      var hourlyRate = Session.get('hourly-rate-' + this._id);
    }
    var amountDue = formatTime * hourlyRate;
    return accounting.formatMoney(amountDue);
  }
});
Template.shift.events({
  "change .arrive": function() {
    Session.set(this.atts.id, $('#this-day').text() + " " + $('#' + this.atts.id).val());
  },
  "change .depart": function() {
    Session.set(this.atts.id, $('#this-day').text() + " " + $('#' + this.atts.id).val());
  },
  "change .rate": function() {
    Session.set(this.atts.id, $('#' + this.atts.id).val());
  },
  "change .rateSelect": function(event, template) {
    console.log("rate changed");
    Session.set(this.atts.id, template.find('#' + this.atts.id).value);
  }
});