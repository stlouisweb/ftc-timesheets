Session.set("today", moment().format('MM/DD/YYYY'))
Template.home.helpers({
  "today": function(){
    return Session.get("today");
  },
  "todayShifts": function() {
     today = Session.get("today");
     yesterday = moment(today).subtract(1, 'days').format('MM/DD/YYYY');
     tomorrow = moment(today).add(1, 'days').format('MM/DD/YYYY');
    if(Meteor.user())
      Meteor.call('addTodaysShifts', Meteor.userId(), today);

    var todaysShifts = Shifts.find({User: Meteor.userId(), Day: {$gt: new Date(today), $lt: new Date(tomorrow)}}, {sort: { Period: 1 }});

      return todaysShifts;
  },
  "weekOf": function() {
    var week = Shifts.findOne({User: Meteor.userId(), Day: {$gt: new Date(today), $lt: new Date(tomorrow)}}).WeekOf;
    return week;
  }
});
Template.home.events({
  "click #prev-day": function(){
    thisDay = Session.get('today');
    prevDay = moment(thisDay).subtract(1, 'days').format('MM/DD/YYYY');
    Session.set("today", prevDay);
  },
  "click #next-day": function(){
    today = $('#next-day').data('today');
    thisDay = Session.get('today');
    nextDay = moment(thisDay).add(1, 'days').format('MM/DD/YYYY');
    Session.set("today", nextDay);
  }
});