Template.users.helpers({
  users: function() {
    return Users.find().fetch();
  }
});

Template.users.events({
  "click .delete": function() {
    user = this;
    bootbox.dialog({
      title: "Delete User",
      message: "Are you sure you want to delete " + this.profile.firstName + " " + this.profile.lastName,
      buttons: {
        danger: {
          label: "YES",
          className: "btn-danger",
          callback: function() {
            console.log(user);
            Users.remove({_id: user._id});
           // bootbox.hideAll();
          }
        },
        info: {
          label: "NO",
          className: "btn-info",
          callback: function() {
            bootbox.hideAll();
          }
        }
      }
    });
  },
  'click .impersonate': function() {
    //console.log(this);
    var userId = this._id;

    Meteor.call('impersonate', userId, function(err) {
      if (!err) {
        console.log('impersonating user');
        Meteor.connection.setUserId(userId);
        Router.go('home');
        Session.set('isImpersonating', true);//surface button or link that reload page - window.reload
        ga("send", "admin", "impersonate");
      }else{
        console.log(err);
      }
    });
  }
});