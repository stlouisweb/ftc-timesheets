AutoForm.hooks({
  insertRatesForm: {
    after: {
      insert: function () {
        Router.go('cities')
      }
    }
  }
});

Template.ratesForm.events({
  'click .cancel': function() {
    Router.go('cities');
  }
})