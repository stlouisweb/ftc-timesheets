Template.week.helpers({
  "thisWeeksShifts": function() {

    var weeksShifts = this.shifts;

    return weeksShifts;
  },

  "totalTime": function() {
    var arrive = moment("1/1/2015 " + this.ArriveTime);
    var depart = moment("1/1/2015 " + this.DepartTime);
    if(arrive && depart){
      var totalTime = depart.diff(arrive);
      if(moment.duration(totalTime, 'milliseconds').hours()){
        var diff = moment.duration(totalTime, 'milliseconds').hours() + " hours and " + moment.duration(totalTime, 'milliseconds').minutes() + " minutes";
      }else{
        var diff = moment.duration(totalTime, 'milliseconds').minutes() + " minutes";
      }
      return diff;
    }else{
      var diff = "n/a";
      return diff;
    }

  }
});