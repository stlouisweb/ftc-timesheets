date = moment();
begin = moment(date).startOf('week').isoWeekday(1);
end = begin.add('d', 6);
endDay = end.format('MM/DD/YYYY');
begin = moment(date).startOf('week').isoWeekday(1);
beginDay = begin.format('MM/DD/YYYY');
console.log(begin.toISOString());
console.log(end.toISOString());
beginISO ='ISODate("' + begin.toISOString() + '")';
endISO ='ISODate("' + end.toISOString() + '")';
console.log(beginISO);

Template.userReport.helpers({
  "endOfWeek": function(){
    return beginDay + ' - ' + endDay;
  },
  "week": function() {
    return begin
  },
  "regularHours": function() {
    employee = this.Employee._id;
    console.log(employee);
    shifts = Shifts.find({Day: {$gte:beginISO, $lte:endISO }}).fetch();
    console.log(shifts);
    return shifts;
  }
});
Template.userReport.events({
  "click #prev-week": function(){
    thisWeek = $('#prev-week').data('week');
    prevWeek = begin.subtract('d', 7);
    Session.set("week", prevWeek);
  },
  "click #next-week": function(){
    thisWeek = $('#next-week').data('week');
    thisDay = Session.get('today');
    nextDay = moment(thisDay).add(1, 'days').format('MM/DD/YYYY');
    Session.set("today", nextDay);
  }
});