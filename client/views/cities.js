Template.cities.events({
  "click .delete": function() {
    city = this;
    bootbox.dialog({
      title: "Delete City",
      message: "Are you sure you want to delete " + this.City,
      buttons: {
        danger: {
          label: "YES",
          className: "btn-danger",
          callback: function() {
            console.log(city);
            Rates.remove({_id: city._id});
            // bootbox.hideAll();
          }
        },
        info: {
          label: "NO",
          className: "btn-info",
          callback: function() {
            bootbox.hideAll();
          }
        }
      }
    });
  }
})