AutoForm.hooks({
  insertUserForm: {
    onSuccess: function(){
      toastr.success('User added');
    },
    onError: function(e){
      toastr.error('Error adding user');
      console.log(this);
      console.log(e);
    }
  }
});
