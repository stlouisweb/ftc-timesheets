Router.configure({
  loadingTemplate: 'loading',
  layoutTemplate: 'mainLayout',
  yieldTemplates: {
    mainNav: {
      to: 'mainNav'
    },
    header: {
      to: 'header'
    },
    footer: {
      to: 'footer'
    }
  }
});

//Iron router plugin to ensure user is signed in
AccountsTemplates.configureRoute('ensureSignedIn', {
  template: 'login'    //template shown if user is not signed in
});

Router.plugin('ensureSignedIn', { //Don't require user logged in for these routes
  except: ['login', 'register']   //can use only: too
});

Router.map(function() {

  /*
   COMMON
   */


  this.route('home', {
    path: '/',
    onBeforeAction: function() {
      if (! Meteor.userId()) {
        Router.go('login');
      } else {
        this.next();
        Router.go('home');
      }
    }
  });
  this.route('shift', {
    path: '/shift/:_id',
    data: function() {
        return Shifts.findOne({_id: this.params._id});
    }
  });
  this.route('week', {
    path: '/week/:WeekOf',
    data: function() {
      return {shifts: Shifts.find({WeekOf: this.params.WeekOf}, {sort: { Day: 1 }}).fetch()};
    }
  });
  this.route('login', {
    path: '/login'
  });
  this.route('addUser', {
    path: '/add-user'
  });
  this.route('users', {
    path: '/users'
  });
  this.route('editUser', {
    onBeforeAction: function() {
      if (! Meteor.userId()) {
        Router.go('loading');
      } else {
        this.next();
        Router.go('editUser');
      }
    },
    waitOn: function() {
      if (Meteor.userId())
        return Meteor.subscribe('allUsers');
    },
    path: '/edit-user/:_id',
    data: function() {
      return Users.findOne({_id: this.params._id});
    }
  });
  this.route('userReport', {
    path: '/user-report/:_id',
    data: function() {
      return {
        Employee: Users.findOne({_id: this.params._id}),
        Shifts:  Shifts.find({User: this.params._id, TotalCharge: {$exists: true}}).fetch()
      };
    }
  });
  this.route('ratesForm', {
    path: '/add-city'
  });
  this.route('cities', {
    path: '/cities',
    data: function() {
      return {
        Cities: Rates.find()
      };
    }
  })
});

usersController = RouteController.extend({

  data: function() {
    return {
      users: Users.find()
    };
  }
});
