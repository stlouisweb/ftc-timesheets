AccountsTemplates.configure({
  forbidClientAccountCreation: true
});
var pwd = AccountsTemplates.removeField('password');
AccountsTemplates.removeField('email');
AccountsTemplates.addFields([
  {
    _id: "username",
    type: "text",
    displayName: "username",
    required: true,
    minLength: 4,
  },
  {
    _id: 'email',
    type: 'email',
    required: true,
    displayName: "email",
    re: /.+@(.+){2,}\.(.+){2,}/,
    errStr: 'Invalid email',
  },
  pwd
]);

AccountsTemplates.configure({
  texts: {
    errors: {
      mustBeLoggedIn: "",
    }
  }
});
//AccountsTemplates.addField({
//  _id: 'firstName',
//  type: 'text',
//  displayName: 'First Name',
//  required: true,
//  func: function(value){
//    return;
//  }
//});
//AccountsTemplates.addField({
//  _id: 'lastName',
//  type: 'text',
//  displayName: 'Last Name',
//  required: true,
//  func: function(value){
//    return;
//  }
//});
