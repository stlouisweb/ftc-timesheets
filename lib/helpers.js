getUserName = function(user){
  return user.username;
};

Rates.helpers({
  regRate: function() {
    return accounting.formatMoney(this.Regular);
  },
  primeRate: function() {
    return accounting.formatMoney(this.Prime);
  }
});