Rates = new Meteor.Collection('rates');

Rates.attachSchema(new SimpleSchema({
  City: {
    type: String
  },
  Regular: {
    type: Number,
    label: 'Regular Rate'
  },
  Prime: {
    type: Number,
    label: 'Prime Rate'
  }
})
);