//TimeSheets = new Meteor.Collection('timesheets');
//
//TimeSheets.attachSchema(new SimpleSchema({
//    Shifts: {
//      type: [String]
//    },
//    User: {
//      type: String,
//      autoValue: function() {
//        if (this.isInsert) {
//          return Meteor.userId();
//        }
//        else if (this.isUpdate) {
//          return Meteor.userId();
//        }
//      },
//      denyUpdate: true,
//      optional: true
//    },
//    // Force value to be current date (on server) upon insert
//    // and prevent updates thereafter.
//    createdAt: {
//      type: Date,
//      autoValue: function() {
//        if (this.isInsert) {
//          return new Date();
//        } else if (this.isUpsert) {
//          return {$setOnInsert: new Date()};
//        } else {
//          this.unset();
//        }
//      },
//      denyUpdate: true
//    },
//    // Force value to be current date (on server) upon update
//    // and don't allow it to be set upon insert.
//    updatedAt: {
//      type: Date,
//      autoValue: function() {
//        if (this.isUpdate) {
//          return new Date();
//        }
//      },
//      denyInsert: true,
//      optional: true
//    }
//  })
//);
//
//TimeSheets.allow({
//  insert: function(userId, doc) {
//    return true;
//  },
//  update: function(userId, doc, fieldNames, modifier) {
//    return true;
//  },
//  remove: function(userId, doc) {
//    return true;
//  }
//});