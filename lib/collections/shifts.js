Shifts = new Meteor.Collection('shifts');

Shifts.attachSchema(new SimpleSchema({
    Day: {
      type: Date,
      denyUpdate: true
    },
    WeekOf: {
      type: String
    },
    TimePeriod: {
      type: String,
      denyUpdate: true
    },
    Period: {
        type: Number,
        denyUpdate: true
    },
    ArriveTime: {
      type: String,
      optional: true
    },
    DepartTime: {
      type: String,
      optional: true
    },
    HourlyRate: {
      type: Number,
      optional: true,
      label: "Hourly Rate Charged",
      autoform: {
        type: "select",
        options: function () {
          if (Meteor.isClient) {
            var cityId = Rates.findOne({_id: Meteor.user().profile.city});
              return [
                {label: "Regular: " + accounting.formatMoney(cityId.Regular) + "/hour", value: cityId.Regular},
                {label: "Prime: " + accounting.formatMoney(cityId.Prime) + "/hour", value: cityId.Prime}
              ];
          }
        }
      }
    },
    FormOfPayment: {
      type: [String],
      autoform: {
        options: [
          {label: "Check", value: "check"},
          {label: "Cash", value: "cash"},
          {label: "Gift Cert", value: "gift_cert"},
          {label: "Card", value: "card"}
        ]
      },
      optional: true
    },
    CheckNumber: {
      type: String,
      optional: true
    },
    TotalCharge: {
      type: Number,
      optional: true
    },
    TipAmount: {
      type: Number,
      optional: true
    },
    HoldingFee: {
      type: Number,
      optional: true
    },
    CustomerNotes: {
      type: String,
      optional: true
    },
    //DepositVerified: {
    //  type: String,
    //  optional: true
    //},
    User: {
      type: String,
      denyUpdate: true,
      optional: true
    },
    // Force value to be current date (on server) upon insert
    // and prevent updates thereafter.
    createdAt: {
      type: Date,
      autoValue: function() {
        if (this.isInsert) {
          return new Date();
        } else if (this.isUpsert) {
          return {$setOnInsert: new Date()};
        } else {
          this.unset();
        }
      },
      denyUpdate: true
    },
    // Force value to be current date (on server) upon update
    // and don't allow it to be set upon insert.
    updatedAt: {
      type: Date,
      autoValue: function() {
        if (this.isUpdate) {
          return new Date();
        }
      },
      denyInsert: true,
      optional: true
    }
  })
);

Shifts.allow({
  insert: function(userId, doc) {
    return true;
  },
  update: function(userId, doc, fieldNames, modifier) {
    return true;
  },
  remove: function(userId, doc) {
    return true;
  }
});